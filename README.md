# インタフェースの扱い方

.NET でインタフェースの扱い方について

## 課題

現在の機能に乗算 (multiplication)、除算 (division)を追加してください。  
既存のコードへの影響度やテスト範囲、作業分担しやすいかどうか考えながら実装してみてください。  
インタフェースとクラス分けをしない実装とも比較してみてください。  


License
=======

Microsoft Public License (MS-PL)

http://opensource.org/licenses/MS-PL
